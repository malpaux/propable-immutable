/**
 * Prop type definitions
 * @module propable-immutable/proptypes
 * @license Copyright (c) 2017 Malpaux IoT All Rights Reserved.
 */

import { List as IMList, Map as IMMap } from 'immutable';
import PropType from 'propable/proptype';

/**
 * Key/value map of all propTypes
 * @alias module:propable-immutable/proptypes
 */
const propTypes = {};

/** Immutable map prop type */
export const Map = new PropType('IMMAP',
  (v) => {
    // if (IMMap.isMap(v)) return v;

    let res = v;
    if (typeof res === 'string') {
      try {
        res = JSON.parse(res);
      } catch (err) { /* continue */ }
    }

    if (typeof res === 'object') {
      try {
        return IMMap(res);
      } catch (err) { /* continue */ }
    }

    throw new Error();
  }, { label: 'Map' }).register(propTypes);

/** Immutable list prop type */
export const List = new PropType('IMLIST',
  (v) => {
    if (IMList.isList(v)) return v;
    if (IMMap.isMap(v)) return v.toList();

    let res = v;
    if (typeof res === 'string') {
      try {
        res = JSON.parse(res);
      } catch (err) { /* continue */ }
    }

    if (Object.prototype.toString.call(res) === '[object Array]') {
      try {
        return IMList(res);
      } catch (err) { /* continue */ }
    }

    throw new Error();
  }, { label: 'List' }).register(propTypes);

export default propTypes;
