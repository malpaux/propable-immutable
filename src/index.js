/**
 * @file Master entrypoint
 */

/**
 * @module propable-immutable
 * @license Copyright (c) 2017 Malpaux IoT All Rights Reserved.
 */

import PropType from 'propable/proptype';

import list, * as propTypes from './proptypes';

/**
 * Add additional prop types
 * @param {Object<string, PropType>} types - Additional prop types
 * @return {Object<string, PropType>} All prop types
 */
const extend = (types) => {
  for (const k in types) { // eslint-disable-line no-restricted-syntax
    if (Object.prototype.hasOwnProperty.call(types, k)) {
      const type = types[k];
      if (PropType.isPropType(type)) propTypes[k] = type.register(list);
    }
  }
  return propTypes;
};

module.exports = {
  propTypes,
  list,
  extend,
};
