/**
 * @file Prop types test suite
 * @license Copyright (c) 2017 Malpaux IoT All Rights Reserved.
 */

import { List, Map } from 'immutable';

import * as propTypes from './proptypes';

describe('propTypes', () => {
  it('should parse to the map prop type', () => {
    const o = { key: 'value' },
      m = Map(o);

    expect(propTypes.Map.parse(m)).toBe(m);
    expect(propTypes.Map.parse(o).toJS()).toEqual(o);
    expect(propTypes.Map.parse(JSON.stringify(o)).toJS()).toEqual(o);
    expect(propTypes.Map.parse('not an object')).toBe();
  });

  it('should parse to the list prop type', () => {
    const a = ['v1', 'v2'],
      l = List(a);

    expect(propTypes.List.parse(l)).toBe(l);
    expect(propTypes.List.parse(a).toJS()).toEqual(a);
    expect(propTypes.List.parse(JSON.stringify(a)).toJS()).toEqual(a);
    expect(propTypes.List.parse('not an array')).toBe();
  });
});
