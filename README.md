# Propable-Immutable [![wercker status](https://app.wercker.com/status/c6f879a1e3fc37b096d713ac740e9307/s/master "wercker status")](https://app.wercker.com/project/byKey/c6f879a1e3fc37b096d713ac740e9307)

> A collection of immutable.js prop types extending [```propable```](https://www.npmjs.com/package/propable).
>
> **[API Reference](https://bitbucket.org/malpaux/propable-immutable/wiki/Home)**


## Installing / Getting Started

Install the package
```shell
npm install --save propable-immutable
```

and import/require it
```javascript
import { propTypes } from 'propable-immutable';
// OR (pre ES6)
var propTypes = require('propable-immutable').propTypes;
```

### Usage
```javascript
const o = { key: 'value' };

propTypes.Map.test(o); // true
propTypes.Map.parse(o); // Map { key: 'value' }
propTypes.Map.parse(JSON.stringify(o)); // Map { key: 'value' }

propTypes.List.test(o); // false
```

## Developing

This is what you do after you have cloned the repository:

```shell
npm install
npm run build
```

(Install dependencies & build the project.)

### Linting

Execute ESLint

```shell
npm run lint
```

Try to automatically fix linting errors
```shell
npm run lint:fix
```

### Testing

Execute Jest unit tests using

```shell
npm test
```

Tests are defined in the same directory the module lives in. They are specified in '[module].test.js' files.

### Building

To build the project, execute

```shell
npm run build
```

This saves the production ready code into 'dist/'.
