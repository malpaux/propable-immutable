var path = require('path'),
  fs = require('fs');

var appDirectory = fs.realpathSync(process.cwd());
function resolveApp(relativePath) {
  return path.resolve(appDirectory, relativePath);
}

var nodePaths = (process.env.NODE_PATH || '')
  .split(process.platform === 'win32' ? ';' : ':')
  .filter(Boolean)
  .map(resolveApp);

  module.exports = {
    appName: 'immutable-box',
    appBuild: resolveApp('dist'),
    appSrc: resolveApp('src/'),
    appJs: {
      index: resolveApp('src/index.js'),
    },

    appPackageJson: resolveApp('package.json'),
    appNodeModules: resolveApp('node_modules'),
    ownNodeModules: resolveApp('node_modules'),
    nodePaths: nodePaths
  };
